package com.starbucks.digitalcontent.sdl.deployer;

import com.sun.jna.platform.win32.Kernel32;

public interface AffinityKernel extends Kernel32 {
    public boolean SetProcessAffinityMask(HANDLE hProcess, int dwProcessAffinityMask);
}

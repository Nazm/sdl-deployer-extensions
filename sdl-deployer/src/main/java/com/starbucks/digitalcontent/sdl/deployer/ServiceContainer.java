package com.starbucks.digitalcontent.sdl.deployer;

import java.util.Arrays;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinNT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Service;

@Service
@EnableAutoConfiguration(exclude = {
    org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration.class, 
    org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration.class, 
    org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration.class, 
    org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration.class}
)
@ImportResource({"classpath*:/META-INF/*/service-*.xml"})
@ComponentScan({
    "com.sdl.delivery.spring.configuration", 
    "com.sdl.web.spring.configuration"}
)
public final class ServiceContainer
{
  private static final Logger LOG = LoggerFactory.getLogger(ServiceContainer.class);
  
  public static void main(String[] args) {
    LOG.info("Starting CD service container wrapper with parameters {}", Arrays.toString(args));

    int pid = -1;
    AffinityKernel instance = (AffinityKernel) Native.loadLibrary("Kernel32",AffinityKernel.class);
    System.out.println(instance.SetProcessAffinityMask(new WinNT.HANDLE(Pointer.createConstant(pid)), 1));


    SpringApplication springApplication = new SpringApplication(new Object[] {
        com.sdl.delivery.service.ServiceContainer.class 
    });
    springApplication.setShowBanner(true);
    springApplication.run(args);
    LOG.info("CD service container started");
  }
}
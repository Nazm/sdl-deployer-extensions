# sdl-deployer-extensions
Deployer extensions for SDL Web 8.1.1


### azure-functions
Azure Functions to consume and process Azure Service Bus notification messages.

### sdl-deployer
Standalone SDL Tridion Deployer instance to debug and test deployer modules locally.

### sdl-deployer-notifications
Deployer module to post content publish/unpublish notifications to Azure Service Bus.

### Instructions to run the program
![Image of Yaktocat](https://bitbucket.org/Nazm/all_repo_image/src/master/sbux_de-azure/sdl-deployer-extensions/readme/1.PNG)
![Image of Yaktocat](https://bitbucket.org/Nazm/all_repo_image/src/master/sbux_de-azure/sdl-deployer-extensions/readme/2.PNG)
![Image of Yaktocat](https://bitbucket.org/Nazm/all_repo_image/src/master/sbux_de-azure/sdl-deployer-extensions/readme/3.PNG)
![Image of Yaktocat](https://bitbucket.org/Nazm/all_repo_image/src/master/sbux_de-azure/sdl-deployer-extensions/readme/4.PNG)
![Image of Yaktocat](https://bitbucket.org/Nazm/all_repo_image/src/master/sbux_de-azure/sdl-deployer-extensions/readme/5.PNG)
![Image of Yaktocat](https://bitbucket.org/Nazm/all_repo_image/src/master/sbux_de-azure/sdl-deployer-extensions/readme/6.PNG)
![Image of Yaktocat](https://bitbucket.org/Nazm/all_repo_image/src/master/sbux_de-azure/sdl-deployer-extensions/readme/7.PNG)
![Image of Yaktocat](https://bitbucket.org/Nazm/all_repo_image/src/master/sbux_de-azure/sdl-deployer-extensions/readme/8.PNG)



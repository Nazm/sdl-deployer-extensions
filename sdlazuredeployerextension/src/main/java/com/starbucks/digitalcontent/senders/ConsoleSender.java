/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.senders;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.starbucks.digitalcontent.model.DeploymentNotification;
import com.starbucks.digitalcontent.model.gson.CollectionAdapter;
import com.tridion.configuration.Configuration;
import java.util.Collection;
import java.util.Map;
import org.slf4j.Logger;

/**
 *
 * @author pebeemst
 */
public class ConsoleSender implements INotificationSender {

    private Logger _log;
    
    @Override
    public void initialize(Configuration config) {

    }

    @Override
    public boolean send(DeploymentNotification data, String transactionId) {
        return this.send(data, transactionId, null);
    }

    @Override
    public boolean send(DeploymentNotification data, String transactionId, Map<String, Object> properties) {
        
        _log.debug("Sending notification message {}", transactionId);
        
        Gson gson = new GsonBuilder()
            .registerTypeHierarchyAdapter(Collection.class, new CollectionAdapter())
            .create();
        String json = gson.toJson(data);
        System.out.println(json);
        
        _log.debug(json);
        _log.debug("with properties:");
        if(null!=properties) {
            properties.forEach((key,value) -> {
                _log.debug("  [ {} = {} ]", key, value);
            });
        } else {
            _log.debug("  [null]");
        }
        _log.debug("Notification sent.");
        return true;
    }

    @Override
    public void setLogger(Logger instance) {
        this._log = instance;
    }

}

package com.starbucks.digitalcontent.senders;

import com.google.gson.reflect.TypeToken;
import com.microsoft.aad.adal4j.AuthenticationContext;
import com.microsoft.aad.adal4j.AuthenticationResult;
import com.microsoft.aad.adal4j.ClientCredential;
import com.microsoft.azure.keyvault.KeyVaultClient;
import com.microsoft.azure.keyvault.authentication.KeyVaultCredentials;
import com.microsoft.azure.keyvault.models.SecretBundle;
import com.microsoft.azure.servicebus.*;
import com.microsoft.azure.servicebus.primitives.ConnectionStringBuilder;
import com.google.gson.Gson;

import static java.nio.charset.StandardCharsets.*;

import java.net.MalformedURLException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.*;

//import org.apache.commons.*;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;
import com.sdl.delivery.configuration.Configuration;
import com.sdl.delivery.configuration.ConfigurationException;
import com.sdl.delivery.configuration.utils.ConfigurationPropertyUtils;
import com.sdl.delivery.configuration.xml.XMLConfigurationReader;
import com.sdl.delivery.configuration.xml.XMLConfigurationReaderImpl;
import org.slf4j.Logger;

public class AzureServiceBusSender2 {
    Logger _log;
    String vaultBase = "https://sbux-akamai-api-creds2.vault.azure.net/";
    String clientId = "a88cdf03-c346-47b5-8011-28f1f5abf94a"; // Client ID from Az active dir
    String clientKey = "bg/10-I@QWks1.ByPAv6puzgVgh=LBSb";  //Client Secret from Az active dir

    public AzureServiceBusSender2(Logger _log){
        this._log = _log;
        //will be getting above creds from azure_conf.xml
    }

    public void send(String jsonInString) {
        Map<String, String> kvSecretValuePair = keyVaultdata(_log);
        QueueClient sendClient = null;
        try {
            sendClient = new QueueClient(new ConnectionStringBuilder(kvSecretValuePair.get("ServiceBusConnectionString"), kvSecretValuePair.get("MainQueueName")), ReceiveMode.PEEKLOCK);
        } catch (Exception e) {
            _log.error("Could not Initialize Azure ServiceBus queue client [{}]", e);
        }
        Message message = new Message(jsonInString.getBytes(UTF_8));
        message.setContentType("application/json");
        try {
            sendClient.send(message);
            sendClient.close();
        } catch (Exception e) {
            _log.error("Exception sending message [{}]", e);
        }
    }

    private Map keyVaultdata(Logger _log){
        Map<String, String> kvSecretValuePair = new HashMap< String, String>();
        KeyVaultClient keyVaultClient = new KeyVaultClient(new KeyVaultCredentials(){
            @Override
            public String doAuthenticate(String authorization, String resource, String scope) {
            String token = null;
            try {
                AuthenticationResult authResult = getAccessToken(authorization, resource);
                token = authResult.getAccessToken();
            } catch (Exception e) {
                _log.error("Exception acquiring access token [{}]", e);
            }
            return token;
            }
        });
        //SecretBundle bundle = keyVaultClient.getSecret(vaultBase, "MainQueueName");
        kvSecretValuePair.put("MainQueueName", keyVaultClient.getSecret(vaultBase, "MainQueueName").value());
        kvSecretValuePair.put("ServiceBusConnectionString", keyVaultClient.getSecret(vaultBase, "ServiceBusConnectionString").value());

       return kvSecretValuePair;
    }

    private AuthenticationResult getAccessToken(String authorization, String resource) throws InterruptedException, ExecutionException, MalformedURLException {

        AuthenticationResult result = null;
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(1);
            AuthenticationContext context = new AuthenticationContext(authorization, false, service);
            Future<AuthenticationResult> future = null;
            if (clientKey != null && clientKey != null) {
                ClientCredential credentials = new ClientCredential(clientId, clientKey);
                future = context.acquireToken(resource, credentials, null);
            }
            result = future.get();
        } finally {
            service.shutdown();
        }
        if (result == null) {
            throw new RuntimeException("Authentication results were null.");
        }
        return result;
    }

}

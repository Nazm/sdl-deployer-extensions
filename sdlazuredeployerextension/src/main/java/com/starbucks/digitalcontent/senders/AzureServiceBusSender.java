/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.senders;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.microsoft.azure.servicebus.ClientFactory;
import com.microsoft.azure.servicebus.IMessage;
import com.microsoft.azure.servicebus.IMessageSender;
import com.microsoft.azure.servicebus.Message;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;
import com.starbucks.digitalcontent.model.DeploymentNotification;
import com.starbucks.digitalcontent.model.gson.CollectionAdapter;
import com.tridion.configuration.Configuration;
import com.tridion.configuration.ConfigurationException;
import static java.nio.charset.StandardCharsets.UTF_8;
import java.util.Collection;
import java.util.Map;
import org.slf4j.Logger;

/**
 *
 * @author pebeemst
 */
public class AzureServiceBusSender implements INotificationSender {

    private Logger _log;
    private String _connectionString;

    @Override
    public void initialize(Configuration config) {
        if (null == config) {
            throw new IllegalArgumentException("Config cannot be null.");
        }
        try {
            Configuration conn = config.getChild("ConnectionString");
            if (null != conn) {
                _connectionString = conn.getContent();
            } else {
                throw new ConfigurationException("No connection string found.");
            }
        } catch (ConfigurationException ex) {
            if(null!=_log) {
                _log.warn("An exception occurred initializing the Azure Service Bus sender: {}", ex.getLocalizedMessage());
            }
        }
    }

    @Override
    public boolean send(DeploymentNotification data, String transactionId) {
        return this.send(data, transactionId, null);
    }

    @Override
    public boolean send(DeploymentNotification data, String transactionId, Map<String, Object> properties) {
        
        try {
            Gson gson = new GsonBuilder()
                .registerTypeHierarchyAdapter(Collection.class, new CollectionAdapter())
                .create();
            String json = gson.toJson(data);

            IMessageSender client = 
                    ClientFactory.createMessageSenderFromConnectionString(_connectionString);
            IMessage message = new Message(json.getBytes(UTF_8));
            message.setContentType("application/json");
            message.setCorrelationId(transactionId);
            if(null != properties) {
                message.setProperties(properties);
            }
            client.send(message);
            client.close();

            return true;

        } catch(InterruptedException | ServiceBusException e) {
            _log.warn("An exceptio occurred sending notification to Azure Service Bus: {}", e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public void setLogger(Logger instance) {
        this._log = instance;
    }
    
}

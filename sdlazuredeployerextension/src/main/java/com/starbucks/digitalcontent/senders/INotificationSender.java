/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.senders;

import com.starbucks.digitalcontent.model.DeploymentNotification;
import com.tridion.configuration.Configuration;
import java.util.Map;
import org.slf4j.Logger;

/**
 *
 * @author pebeemst
 */
public interface INotificationSender {
    public void initialize(Configuration config);
    public boolean send(DeploymentNotification data, String transactionId);
    public boolean send(DeploymentNotification data, String transactionId, Map<String, Object> properties);
    public void setLogger(Logger instance);
}

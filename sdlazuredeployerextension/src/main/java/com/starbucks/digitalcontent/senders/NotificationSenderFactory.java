/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.senders;

import com.tridion.configuration.Configuration;
import com.tridion.configuration.ConfigurationException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;

/**
 *
 * @author pebeemst
 */
public class NotificationSenderFactory {
    public static List<INotificationSender> getInstances(Configuration config, Logger logger) {
        
        List<INotificationSender> senders = Collections.synchronizedList(new ArrayList<>());
        List<Configuration> configs = config.getChildrenByName("Sender");
        
        configs.forEach((sender) -> {
            try {
                String className = null!=sender ? sender.getAttribute("Class") : null;

                if(null!=className && !"".equals(className)) {
                    Class<?> c = Class.forName(className);
                    INotificationSender instance = (INotificationSender) c.getDeclaredConstructor().newInstance();
                    instance.setLogger(logger);
                    instance.initialize(sender);
                    senders.add(instance);
                }   
            } catch (ConfigurationException 
                    | ClassNotFoundException 
                    | NoSuchMethodException 
                    | SecurityException 
                    | InstantiationException 
                    | IllegalAccessException 
                    | IllegalArgumentException 
                    | InvocationTargetException ex) {
                logger.error("Failed to instantiate sender instance: ", ex.getLocalizedMessage());
            }
        });
        return senders;
    }
}

package com.starbucks.digitalcontent;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.starbucks.digitalcontent.model.Binary;
import com.starbucks.digitalcontent.model.ComponentPresentation;
import com.starbucks.digitalcontent.model.DeploymentNotification;
import com.starbucks.digitalcontent.model.Keyword;
import com.starbucks.digitalcontent.model.Page;
import com.starbucks.digitalcontent.model.Publication;
import com.starbucks.digitalcontent.model.Schema;
import com.starbucks.digitalcontent.tridion.ContentClientFacade;
import com.starbucks.digitalcontent.tridion.ContentClientFactory;
import com.starbucks.digitalcontent.tridion.DiscoveryClient;
import com.starbucks.digitalcontent.tridion.DiscoveryClientFactory;
import com.tridion.util.TCMURI;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class App {

    static class CollectionAdapter implements JsonSerializer<List<?>> {
        @Override
        public JsonElement serialize(List<?> src, Type typeOfSrc, JsonSerializationContext context) {
            if(null == src || src.isEmpty()) {
                return null;
            }
            JsonArray array = new JsonArray();
            src.forEach((child)-> {
                JsonElement element = context.serialize(child);
                array.add(element);
            });
            return array;
        }
    }
    
    public static void main(String[] args) {
        
        boolean verbose = true;
        
        TCMURI binaryURI = new TCMURI(14,145,16,0); // nl-nl publish settings
        
        List<TCMURI> pageURIs = Collections.synchronizedList(new ArrayList<>());
        pageURIs.add(new TCMURI(181,37050,64,0)); // Monarch e-mail page
        pageURIs.add(new TCMURI(140,34555,64,0)); // Rewards homepage
        pageURIs.add(new TCMURI(140,31066,64,0)); // Careers homepage
        pageURIs.add(new TCMURI(140,31448,64,0)); // Reserve 
        
        TCMURI componentURI = new TCMURI(181,37052,16,0); // monarch test component
        TCMURI templateURI = new TCMURI(181,37053,32,0); // monarch test template
        TCMURI categoryURI = new TCMURI(181,6564,512,0); // monarch audience type
        
        TCMURI schemaURI = new TCMURI(140,30386,8,0); // nav config

        DeploymentNotification notification = new DeploymentNotification();
        notification.setAction("development");
        notification.setTransactionId(java.time.OffsetDateTime.now().toString());

        ContentClientFacade content = new ContentClientFacade(ContentClientFactory.getInstance(), verbose);
        DiscoveryClient discovery = DiscoveryClientFactory.getInstance();
        List<URI> baseURLs = discovery.getWebApplicationBaseURLs(140);
        
        Publication publ = new Publication();
        publ.setId("tcm:0-test-1");
        publ.setTitle("Test publication.");
        publ.setKey("test/us/en");
        publ.getBaseURIs().addAll(baseURLs);
        
        notification.setPublication(publ);
        
        for(int i=0; i<1; i++) {

            List<Binary> binaries = content.getBinaryVariants(binaryURI);
            if(null != binaries) {
                notification.getBinaries().addAll(binaries);
            }

            pageURIs.forEach((TCMURI uri) -> {
                App.print("Getting page " + uri.toString());
                Page page = content.getPage(uri);
                if(null!=page) {
                    notification.getPages().add(page);
                } else {
                    App.warn("Did not retrieve page " + uri.toString());
                }
            });

            ComponentPresentation cp = content.getComponentPresentation(componentURI, templateURI);
            if(null != cp) {
                notification.getComponentPresentations().add(cp);
            } else {
                App.warn("Did not retrieve component presentation [component='" + componentURI.toString() + "', template='"+ templateURI.toString()+"']");
            }
            
            Keyword taxonomy = content.getTaxonomy(categoryURI);
            if(null != taxonomy) {
                notification.getTaxonomies().add(taxonomy);
            } else {
                App.warn("Did not retrieve taxonomy " + categoryURI.toString());
            }
            
            Schema xsd = content.getSchema(schemaURI);
            if(null!=xsd) {
                notification.getContentTypes().add(xsd);
            } else {
                App.warn("Did not retrieve schema " + schemaURI.toString());
            }
        }
        Gson gson = new GsonBuilder()
            .registerTypeHierarchyAdapter(Collection.class, new App.CollectionAdapter())
            .create();

        App.print("");
        App.print(gson.toJson(notification));
        
    }
    
    private static void print(String message) {
        System.out.println(message);
    }
    
    private static void warn(String message) {
        System.err.println(message);
    }
}

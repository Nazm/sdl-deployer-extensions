package com.starbucks.digitalcontent.tridion.deployer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.starbucks.digitalcontent.model.Binary;
import com.starbucks.digitalcontent.tridion.ContentClientFacade;
import com.tridion.transport.transportpackage.TransportPackage;
import com.tridion.util.TCMURI;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class TransportPackageHandler {

	public String GetJsonContent(String path, String type) {
		
		String commaSeperatedList = "";
		try (Stream<Path> walk = Files.walk(Paths.get(path),1)) {// walks the dir for 1 level

    		List<String> result = walk.filter(Files::isRegularFile)
    				.map(x -> x.toString()).collect(Collectors.toList());
    		
    		result.replaceAll(item -> {
				try {
					return new String(Files.readAllBytes(Paths.get(item)));
				} catch (IOException e) {
					e.printStackTrace();
				}
				return item;
				//return XML.toJSONObject(item).toString();
			});
    		commaSeperatedList = String.join(",", result);
    					
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
		return "\""+type+"\" : ["+ commaSeperatedList +"] ";
	}
	
	
	public List<String> GetContent(String basePath, List<String> lookIntoFolders) {
        List<String> lookIntoFolderContents = new ArrayList<String>();
        lookIntoFolders.forEach(item ->{
			File tmpDir = new File(basePath+"\\"+item);
        	if(tmpDir.exists())
        		lookIntoFolderContents.add(GetJsonContent(basePath+"\\"+item, item));
        });
		return lookIntoFolderContents;
	}


}

package com.starbucks.digitalcontent.tridion;

import com.sdl.odata.client.ODataClientFactoryImpl;
import com.sdl.odata.client.ODataV4ClientComponentsProvider;
import com.sdl.odata.client.URLConnectionRequestPropertiesBuilder;
import com.sdl.odata.client.api.ODataActionClientQuery;
import com.sdl.odata.client.api.ODataClient;
import com.sdl.odata.client.api.ODataClientFactory;
import com.sdl.odata.client.api.ODataClientQuery;
import com.sdl.odata.client.api.exception.ODataClientException;
import com.sdl.odata.client.api.exception.ODataClientNotAuthorized;
import com.sdl.odata.client.api.exception.ODataClientRuntimeException;
import com.sdl.odata.client.api.exception.ODataClientSocketException;
import com.sdl.odata.client.api.exception.ODataClientTimeout;
import com.sdl.web.client.configuration.api.ConfigurationException;
import com.sdl.web.client.impl.OAuthTokenProvider;
import com.sdl.web.content.client.ODataV2ClientComponentsProvider;
import com.sdl.web.content.client.cache.CacheProvider;
import com.sdl.web.content.client.configuration.ContentClientConfigurationLoader;
import com.sdl.web.content.client.versions.ContentVersionManager;
import com.tridion.ambientdata.CookieConfig;
import com.tridion.ambientdata.claimstore.ClaimStore;
import com.tridion.ambientdata.claimstore.cookie.ClaimCookieSerializer;
import com.tridion.ambientdata.claimstore.cookie.ClaimsCookie;
import com.tridion.ambientdata.web.WebContext;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URI;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Function;
import java.util.function.Supplier;
import javax.cache.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContentClient {

    private static final Logger LOG = LoggerFactory.getLogger(ContentClient.class);
    private static final int ATTEMPT_DELAY_FACTOR = 100;
    private static final int MAX_RETRY_COUNT = 3;
    private final List<String> oDataV4EdmEntityClassNames;
    private final List<String> oDataV2EdmEntityClassNames;
    private final ClaimCookieSerializer claimCookieSerializer;
    private final List<URI> claimsForForwarding;

    private ODataClient oDataV2Client;
    private ODataClient oDataClient;
    private URL oDataClientURL;
    private final CacheProvider cacheProvider;
    private OAuthTokenProvider tokenProvider;
    private final ContentVersionManager versionManager;

    protected ContentClient(ContentClientConfigurationLoader loader, List<String> odataV2EdmClassNames, List<String> odataV4EdmClassNames) throws ConfigurationException {
        Properties cacheConfiguration = loader.getCacheConfiguration();
        this.cacheProvider = CacheProvider.getInstance(cacheConfiguration.getProperty("CacheUri"));
        this.cacheProvider.configure(cacheConfiguration);

        this.oDataV4EdmEntityClassNames = odataV4EdmClassNames;
        this.oDataV2EdmEntityClassNames = odataV2EdmClassNames;

        if (loader.isTokenConfigurationAvailable()) {
            initTokenProvider(loader.getOauthTokenProviderConfiguration());
        }

        ODataClientFactoryImpl oDataClientFactoryImpl = new ODataClientFactoryImpl();
        initODataV2Client(oDataClientFactoryImpl, loader.getContentV2Configuration());
        initODataV4Client(oDataClientFactoryImpl, loader.getContentV4Configuration());

        Optional<Map.Entry<String, List<URI>>> claimForwardConfig = loader.getForwardedClaimsConfiguration().entrySet().stream().findFirst();
        if (claimForwardConfig.isPresent()) {
            this.claimCookieSerializer = new ClaimCookieSerializer((String) ((Map.Entry) claimForwardConfig.get()).getKey());
            this.claimsForForwarding = (List) ((Map.Entry) claimForwardConfig.get()).getValue();
        } else {
            this.claimCookieSerializer = new ClaimCookieSerializer(CookieConfig.CookieType.ADF.getDefaultName());
            this.claimsForForwarding = Collections.emptyList();
        }
        this.versionManager = new ContentVersionManager(this.oDataClientURL.toString(), buildRequestProperties());
    }

    protected Object getEntity(String entityType, ODataClientQuery clientQuery) {
        if (this.cacheProvider.isCacheEnabled()) {
            LOG.trace("Cache is enabled, trying to get entity from cache");
            Cache<ODataClientQuery, Serializable> cache = this.cacheProvider.provideCacheForClass(ODataClientQuery.class, Serializable.class);

            Serializable entity = getFromCache(cache, clientQuery);
            if (entity == null) {
                LOG.trace("No such query in cache, getting from data store");
                entity = (Serializable) getEntityFromDataStore(entityType, clientQuery);
                if (entity != null) {
                    cache.put(clientQuery, entity);
                }
            }
            return entity;
        }
        return getEntityFromDataStore(entityType, clientQuery);
    }

    protected List<?> getEntities(String entityType, ODataClientQuery clientQuery) {
        if (this.cacheProvider.isCacheEnabled()) {
            LOG.trace("Cache is enabled, trying to get entity from cache");
            Cache<ODataClientQuery, Serializable> cache = this.cacheProvider.provideCacheForClass(ODataClientQuery.class, Serializable.class);

            Object entities = getFromCache(cache, clientQuery);
            if (entities == null) {
                LOG.trace("No such query in cache, getting from data store");
                entities = getEntitiesFromStorage(entityType, clientQuery);
                if (entities != null) {
                    cache.put(clientQuery, (Serializable) entities);
                }
            }
            return (List) entities;
        }
        return getEntitiesFromStorage(entityType, clientQuery);
    }

    private Serializable getFromCache(Cache<ODataClientQuery, Serializable> cache, ODataClientQuery clientQuery) {
        LOG.trace("Cache is enabled, trying to get entity from cache");
        Serializable entity = (Serializable) cache.get(clientQuery);
        if (entity == null
                && this.cacheProvider.needCheckHostAvailability() && !this.cacheProvider.getServiceChecker().isHostAvailable()) {
            LOG.trace("Host is not available. Trying to get entity from backup cache");

            Cache<ODataClientQuery, Serializable> backupCache = this.cacheProvider.provideBackupCacheForClass(ODataClientQuery.class, Serializable.class);
            entity = (Serializable) backupCache.get(clientQuery);
            if (entity == null) {
                throw new RuntimeException("The service is not available and cache does not contain value for query: " + clientQuery);
            }

            return entity;
        }

        LOG.trace("Returning entity from cache");
        return entity;
    }

    private Object getEntityFromDataStore(String entityType, ODataClientQuery clientQuery) {
        Function<Map<String, String>, Object> request;
        if (!this.versionManager.isCompatible()) {
            LOG.debug("CIL doesn't support CIS {}", this.versionManager.getCisVersion());
            throw new ODataClientRuntimeException("Unsupported version: " + this.versionManager.getCisVersion());
        }

        if (isV2(entityType, clientQuery)) {
            request = (requestProperties -> {
                try {
                    return this.oDataV2Client.getEntity(requestProperties, clientQuery);
                } catch (ODataClientRuntimeException e) {

                    if (e.getMessage().contains("No resource was found for the requested item")) {
                        return null;
                    }
                    throw e;
                }
            });
        } else {
            request = (requestProperties -> this.oDataClient.getEntity(requestProperties, clientQuery));
        }
        return (new RetryBlock(request, this::buildRequestProperties)).execute();
    }

    private List<?> getEntitiesFromStorage(String entityType, ODataClientQuery clientQuery) {
        return (List) (new RetryBlock(
                isV2(entityType, clientQuery) ? (requestProperties
                -> this.oDataV2Client.getEntities((Map<String, String>) requestProperties, clientQuery)) : (requestProperties
                -> this.oDataClient.getEntities((Map<String, String>) requestProperties, clientQuery)), this::buildRequestProperties))
                .execute();
    }

    private boolean isV2(String entityType, ODataClientQuery clientQuery) {
        if (oDataV4EdmEntityClassNames.contains(entityType) || clientQuery instanceof com.sdl.odata.client.FunctionImportClientQuery || clientQuery instanceof com.sdl.odata.client.BoundFunctionClientQuery) {

            return false;
        }
        if (oDataV2EdmEntityClassNames.contains(entityType)) {
            return true;
        }
        throw new ODataClientRuntimeException("No entity defined for the specified entity type : " + entityType);
    }

    private Object performAction(ODataActionClientQuery actionQuery) {
        return performAction(actionQuery, false);
    }

    private Object performAction(ODataActionClientQuery actionQuery, boolean isCached) {
        if (!this.versionManager.isCompatible()) {
            LOG.debug("CIL doesn't support CIS {}", this.versionManager.getCisVersion());
            throw new ODataClientRuntimeException("Unsupported version: " + this.versionManager.getCisVersion());
        }
        if (isCached && this.cacheProvider.isCacheEnabled()) {
            LOG.trace("Cache is enabled, trying to get entity from cache");
            Cache<ODataClientQuery, Serializable> cache = this.cacheProvider.provideCacheForClass(ODataClientQuery.class, Serializable.class);

            Object entity = getFromCache(cache, actionQuery);
            if (entity == null) {
                LOG.trace("No such query in cache, getting from data store");

                entity = (new RetryBlock(x -> this.oDataClient.performAction((Map<String, String>) x, actionQuery), this::buildRequestProperties)).execute();
                if (entity != null) {
                    cache.put(actionQuery, (Serializable) entity);
                }
            }
            return entity;
        }

        return (new RetryBlock(requestProperties -> this.oDataClient.performAction((Map<String, String>) requestProperties, actionQuery), this::buildRequestProperties)).execute();
    }

    private InputStream getInputStream(URL url) {

        return (InputStream) (new RetryBlock((Object requestProperties) -> {
            try {
                return this.oDataClient.getInputStream((Map<String, String>) requestProperties, url);
            } catch (ODataClientException e) {
                LOG.error("Could not perform the request.", e);
                throw new ODataClientRuntimeException("Could not perform the request.", e);
            }
        }, this::buildRequestProperties
        )).execute();
    }

    private void initODataV2Client(ODataClientFactory clientFactory, Properties serviceConfig) throws ConfigurationException {
        ODataV2ClientComponentsProvider oDataV2ClientComponentsProvider = new ODataV2ClientComponentsProvider(oDataV2EdmEntityClassNames, serviceConfig);

        this.oDataV2Client = clientFactory.create(oDataV2ClientComponentsProvider);

        this.oDataV2Client.encodeURL(false);
    }

    private void initODataV4Client(ODataClientFactory clientFactory, Properties serviceConfig) throws ConfigurationException {
        ODataV4ClientComponentsProvider oDataV4ClientComponentsProvider = new ODataV4ClientComponentsProvider(oDataV4EdmEntityClassNames, serviceConfig);

        this.oDataClient = clientFactory.create(oDataV4ClientComponentsProvider);
        this.oDataClientURL = oDataV4ClientComponentsProvider.getWebServiceUrl();
        this.oDataClient.encodeURL(false);
    }

    private void initTokenProvider(Properties tokenConfiguration) {
        if (this.tokenProvider == null) {
            try {
                this.tokenProvider = new OAuthTokenProvider(tokenConfiguration);
            } catch (Exception e) {
                LOG.error("Error Initializing Token Provider." + e.getMessage());
            }
        }
    }

    private Map<String, String> buildRequestProperties() {
        URLConnectionRequestPropertiesBuilder urlPropsBuilder = new URLConnectionRequestPropertiesBuilder();

        ClaimStore currentClaimStore = WebContext.getCurrentClaimStore();
        if (!this.claimsForForwarding.isEmpty() && currentClaimStore != null) {
            LOG.debug("ForwardedClaims is configured. Will forward following claims: {}", this.claimsForForwarding);
            Map<URI, Object> requiredClaims = new HashMap<>();
            this.claimsForForwarding.stream()
                    .filter(claim -> (currentClaimStore.get(claim) != null))
                    .forEach(claim -> requiredClaims.put(claim, currentClaimStore.get(claim)));

            if (!requiredClaims.isEmpty()) {
                List<ClaimsCookie> claimsCookies = this.claimCookieSerializer.serializeClaims(requiredClaims);
                claimsCookies.stream().forEach(claimsCookie
                        -> urlPropsBuilder.withCookie(claimsCookie.getName(), new String(claimsCookie.getValue())));
            }
        }

        if (this.tokenProvider != null) {
            urlPropsBuilder.withAccessToken(this.tokenProvider.getToken());
        }
        return urlPropsBuilder.build();
    }

    private static class RetryBlock<T> {

        private final Function<Map<String, String>, T> function;

        private final Supplier<Map<String, String>> propertyProvider;
        private int attempt = 0;

        RetryBlock(Function<Map<String, String>, T> function, Supplier<Map<String, String>> propertyProvider) {
            this.function = function;
            this.propertyProvider = propertyProvider;
        }

        T execute() {
            while (this.attempt < 3) {
                this.attempt++;
                try {
                    return (T) this.function.apply(this.propertyProvider.get());
                } catch (ODataClientTimeout e) {
                    retryIfNecessary(e, "Request timed out. Will retry. Attempt #" + this.attempt + ".");
                } catch (ODataClientNotAuthorized e) {
                    retryIfNecessary(e, "Not authorized. Will retry with updated access token. Attempt #" + this.attempt + ".");
                } catch (ODataClientSocketException e) {
                    retryIfNecessary(e, "Could not connect. Will retry. Attempt #" + this.attempt + ".");
                }
            }
            throw new ODataClientRuntimeException("Retry limit exceeded.");
        }

        private void retryIfNecessary(ODataClientRuntimeException e, String msg) {
            if (this.attempt == ContentClient.MAX_RETRY_COUNT) {
                ContentClient.LOG.debug("Retry limit exceeded.", e);
                throw new ODataClientRuntimeException("Retry limit exceeded.", e);
            }
            sleep();
            ContentClient.LOG.debug(msg);
        }

        private void sleep() {
            try {
                Thread.sleep(((this.attempt + 1) * ContentClient.ATTEMPT_DELAY_FACTOR));
            } catch (InterruptedException e) {
                ContentClient.LOG.debug("Retry process was interrupted. Exiting.", e);
            }
        }
    }
}

package com.starbucks.digitalcontent.tridion;

import com.sdl.delivery.configuration.Configuration;
import com.sdl.delivery.configuration.ConfigurationException;
import com.sdl.delivery.configuration.utils.ConfigurationPropertyUtils;
import com.sdl.delivery.configuration.xml.XMLConfigurationReader;
import com.sdl.delivery.configuration.xml.XMLConfigurationReaderImpl;
import com.sdl.odata.client.ODataClientFactoryImpl;
import com.sdl.odata.client.ODataV4ClientComponentsProvider;
import com.sdl.odata.client.api.ODataClient;
import com.sdl.odata.client.api.ODataClientFactory;
import com.sdl.web.client.configuration.CacheSecurityAwareClientPropertiesBuilder;
import com.sdl.web.discovery.datalayer.model.EntitiesRegistry;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DiscoveryClientFactory {

    private static final Logger LOG = LoggerFactory.getLogger(DiscoveryClientFactory.class);
    private static final String STORAGE_CONFIG_FILENAME = "cd_client_conf.xml";

    public static DiscoveryClient getInstance() {

        try {

            XMLConfigurationReader configReader = new XMLConfigurationReaderImpl();
            Configuration clientConfig = configReader.readConfiguration(STORAGE_CONFIG_FILENAME);
            Map<String, String> properties = new HashMap<>();
            clientConfig.getConfigurations().forEach((config) -> {
                try {
                    properties.putAll(ConfigurationPropertyUtils.getConfigurationAsStringValues(config.getValues()).get());
                } catch (ConfigurationException ex) {
                    LOG.warn("Exception occurred reading configuration properties for {}: {}", config.getName(), ex.getLocalizedMessage());
                }
            });

            ODataClientFactory factory = new ODataClientFactoryImpl();
            CacheSecurityAwareClientPropertiesBuilder builder = new CacheSecurityAwareClientPropertiesBuilder(properties);
            List<String> edmEntityClasses = EntitiesRegistry.ALL_EDM_ENTITIES.stream().map(Class::getName).collect(Collectors.toList());
            ODataV4ClientComponentsProvider provider = new ODataV4ClientComponentsProvider(edmEntityClasses, builder.build());
            ODataClient client = factory.create(provider);

            return new DiscoveryClient(client, properties);

        } catch (ConfigurationException ex) {

            LOG.warn(ex.getLocalizedMessage());
            return null;
        }
    }

}

package com.starbucks.digitalcontent.tridion;

import com.sdl.web.client.configuration.api.ConfigurationException;
import com.sdl.web.content.client.configuration.ContentClientConfigurationLoader;
import com.sdl.web.content.client.configuration.impl.ContentServiceClientConfigurationLoader;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContentClientFactory {

    public static final Logger LOG = LoggerFactory.getLogger(ContentClientFactory.class);
    
    public static ContentClient getInstance() {
        try {

            List<String> edmClassNamesV2 = (List) Stream.of(new String[]{
                com.sdl.web.content.client.odata.v2.edm.BinaryContent.class.getName(),
                com.sdl.web.content.client.odata.v2.edm.BinaryMeta.class.getName(),
                com.sdl.web.content.client.odata.v2.edm.BinaryVariant.class.getName(),
                com.sdl.web.content.client.odata.v2.edm.Page.class.getName(),
                com.sdl.web.content.client.odata.v2.edm.Component.class.getName(),
                com.sdl.web.content.client.odata.v2.edm.ComponentPresentation.class.getName(),
                com.sdl.web.content.client.odata.v2.edm.CustomMeta.class.getName(),
                com.sdl.web.content.client.odata.v2.edm.Keyword.class.getName(),
                com.sdl.web.content.client.odata.v2.edm.PageContent.class.getName(),
                com.sdl.web.content.client.odata.v2.edm.StructureGroup.class.getName(),
                com.sdl.web.content.client.odata.v2.edm.KeywordRelation.class.getName(),
                com.starbucks.digitalcontent.tridion.edm.Schema.class.getName(),
                com.starbucks.digitalcontent.tridion.edm.Template.class.getName()
            }).collect(Collectors.toList());

            List<String> edmClassNamesV4 = (List) Stream.of(new String[]{
                com.sdl.web.content.odata.model.Keyword.class.getName(),
                com.sdl.web.content.odata.model.CustomMeta.class.getName(),
                com.sdl.web.content.odata.model.KeywordRelation.class.getName(),
                com.sdl.web.content.odata.model.KeywordURIs.class.getName(),
                com.sdl.web.content.odata.model.PublicationMappingImpl.class.getName(),
                com.sdl.web.content.odata.model.PublicationUrls.class.getName(),
                com.sdl.web.linking.LinkImpl.class.getName(),
                com.sdl.web.content.odata.model.ComponentPresentation.class.getName(),
                com.sdl.web.content.odata.model.ComponentPresentationMeta.class.getName()
            }).collect(Collectors.toList());

            ContentClientConfigurationLoader loader = new ContentServiceClientConfigurationLoader();
            return new ContentClient(loader, edmClassNamesV2, edmClassNamesV4);

        } catch (ConfigurationException ex) {
            LOG.warn(ex.getLocalizedMessage());
            return null;

        }
    }
}

package com.starbucks.digitalcontent.tridion.deployer;

import com.starbucks.digitalcontent.senders.AzureServiceBusSender2;
import com.tridion.configuration.Configuration;
import com.tridion.configuration.ConfigurationException;
import com.tridion.deployer.Module;
import com.tridion.deployer.ProcessingException;
import com.tridion.deployer.Processor;
import com.tridion.transport.transportpackage.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.tridion.util.TCMURI;
import java.io.File;
import com.google.gson.Gson;
import com.starbucks.digitalcontent.model.Binary;
import com.starbucks.digitalcontent.model.DeploymentNotification;
import com.starbucks.digitalcontent.model.Publication;
import com.starbucks.digitalcontent.tridion.ContentClient;
import com.starbucks.digitalcontent.tridion.ContentClientFacade;
import com.starbucks.digitalcontent.tridion.ContentClientFactory;
import com.starbucks.digitalcontent.tridion.DiscoveryClient;
import com.starbucks.digitalcontent.tridion.DiscoveryClientFactory;
import com.tridion.transport.transportpackage.TransportPackage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;



class AbstractDeployerExtension  extends Module {

    protected static Logger _log;


    protected final  DiscoveryClient _discovery;
    protected final  ContentClientFacade _client;

    protected void setLogger(Logger log) {
        this._log = log;
    }

    @SuppressWarnings("deprecation")
    public AbstractDeployerExtension(Configuration config, Processor processor) throws ConfigurationException {
        super(config, processor);

        _log = LoggerFactory.getLogger(this.getClass());

        boolean verbose = config.getBooleanValue("Verbose", false);

        if(verbose) {
            _log.debug("Verbose output format enabled.");
        }

        _log.debug("Initializing SDL Discovery Service client.");
        _discovery = DiscoveryClientFactory.getInstance();

        _log.debug("Initializing SDL Content Interaction client.");
        ContentClient instance = ContentClientFactory.getInstance();
        if(instance == null) { throw new ConfigurationException("SDL Content Interaction client did not initialize. Check logs.");}
        _client = new ContentClientFacade(instance, verbose);

        _log.debug("Initializing notification senders.");
        _log.debug("Deployer module initialized.");
    }



  public void process(TransportPackage tp) throws ProcessingException {

        _log.debug("Processing transport package.");

        ProcessorInstructions instructions = tp.getProcessorInstructions();
        String transactionId = tp.getTransactionId().toString();

        _log.debug("Gathering transaction details.");
        Map<String, Object> properties = getTransactionMetadata(tp);

        _log.debug("Initializing deployment notification message.");
        DeploymentNotification notification = new DeploymentNotification();
        notification.setAction(tp.getAction());
        _log.debug("Adding publication context details.");
        notification.setPublication(getPublicationData(instructions));

        instructions.getSections().forEachRemaining(section -> {
            _log.debug("Processing section [{}]", section.getName());

            if (section.getName().equals("Binaries")){//Binaries
                try {
                    String path = tp.getLocation().getAbsoluteFile()+File.separator+"binaries.xml";
                    File fXmlFile = new File(path);
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(fXmlFile);
                    doc.getDocumentElement().normalize();

                    NodeList nList = doc.getElementsByTagName("Binary");

                    for (int temp = 0; temp < nList.getLength(); temp++) {
                        Node nNode = nList.item(temp);
                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element eElement = (Element) nNode;

                            TCMURI uri = new TCMURI(eElement.getAttribute("Id"));
                            List<Binary> binaries = _client.getBinaryVariants(uri);
                            if(null != binaries && !binaries.isEmpty()) {
                                notification.getBinaries().addAll(binaries);
                            } else {
                                _log.warn(
                                        "Did not retrieve binaries for variant [{}]",
                                        eElement.getAttribute("Id")
                                );
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            else{
                section.getFileItems().forEachRemaining(key -> {
                    _log.debug("Processing key type [{}]", key.getClass().getName());

                    if(key instanceof PageKey) {
                        PageKey pageKey = (PageKey) key;
                        if(!CollectionUtils.addIgnoreNull(
                                notification.getPages(),
                                _client.getPage(pageKey.getId())
                        )) {
                            _log.warn(
                                    "Did not retrieve page [{}]",
                                    pageKey.getId()
                            );
                        }
                    }
                    if(key instanceof ComponentPresentationKey) {
                        ComponentPresentationKey cpKey = (ComponentPresentationKey) key;
                        if(!CollectionUtils.addIgnoreNull(
                                notification.getComponentPresentations(),
                                _client.getComponentPresentation(
                                        cpKey.getComponentId(),
                                        cpKey.getTemplateId()
                                )
                        )) {
                            _log.warn(
                                    "Did not retrieve component presentation [Component='{}', Template='{}']",
                                    cpKey.getComponentId(),
                                    cpKey.getTemplateId()
                            );
                        }
                    }
                    if(key instanceof SchemaKey) {
                        SchemaKey schemaKey = (SchemaKey) key;
                        if(!CollectionUtils.addIgnoreNull(
                                notification.getContentTypes(),
                                _client.getSchema(schemaKey.getId())
                        )) {
                            _log.warn(
                                    "Did not retrieve schema [{}]",
                                    schemaKey.getId()
                            );
                        }
                    }
                    if(key instanceof TaxonomyKey) {
                        TaxonomyKey taxonomyKey = (TaxonomyKey) key;
                        if(!CollectionUtils.addIgnoreNull(
                                notification.getTaxonomies(),
                                _client.getTaxonomy(taxonomyKey.getId())
                        )) {
                            _log.warn(
                                    "Did not retrieve taxonomy [{}]",
                                    taxonomyKey.getId()
                            );
                        }
                    }
                });
            }
        });

        Gson gson = new Gson();
        String jsonInString = gson.toJson(notification);
        AzureServiceBusSender2 azureServiceBusSender = new AzureServiceBusSender2(_log);
        try {
            azureServiceBusSender.send(jsonInString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        _log.debug("Processing complete.");
    }

    private Publication getPublicationData(ProcessorInstructions pi) {

        if(null == pi) { return null; }

        int publicationId = pi.getPublicationId().getItemId();

        Publication result = new Publication();
        result.setKey(pi.getPublication().getKey());
        result.setTitle(pi.getPublication().getTitle());
        result.setId(pi.getPublicationId().toString());
        result.setBaseURIs(_discovery.getWebApplicationBaseURLs(publicationId));


        return result;
    }

    private Map<String, Object> getTransactionMetadata(TransportPackage tp) {
        Map<String, Object> result = new HashMap<>();

        com.tridion.transport.transportpackage.Publication publication = tp.getProcessorInstructions().getPublication();

        result.put("action", tp.getAction());
        result.put("publication", publication.getTitle());
        result.put("publicationID", publication.getPublicationId().getItemId());
        result.put("publicationKey", publication.getKey());
        result.put("publicationURI", publication.getPublicationId().toString());
        result.put("transactionURI", tp.getTransactionId().toString());

        return result;
    }

}

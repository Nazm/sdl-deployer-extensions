package com.starbucks.digitalcontent.tridion.deployer;

import com.tridion.configuration.Configuration;
import com.tridion.configuration.ConfigurationException;
import com.tridion.deployer.Processor;

public class NotificationsDeploy extends AbstractDeployerExtension {
    public NotificationsDeploy(Configuration config, Processor processor) throws ConfigurationException {
        super(config, processor);
    }
}

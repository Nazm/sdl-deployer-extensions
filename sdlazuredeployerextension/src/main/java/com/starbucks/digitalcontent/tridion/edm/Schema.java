package com.starbucks.digitalcontent.tridion.edm;

import com.sdl.odata.api.edm.annotations.EdmEntity;
import com.sdl.odata.api.edm.annotations.EdmEntitySet;
import com.sdl.odata.api.edm.annotations.EdmNavigationProperty;
import com.sdl.odata.api.edm.annotations.EdmProperty;
import com.sdl.web.content.client.odata.v2.edm.Component;
import java.io.Serializable;
import java.util.List;

@EdmEntity(name = "Schema", namespace = "Tridion.ContentDelivery", key = {"schemaId"})
@EdmEntitySet(name = "Schemas")
public class Schema extends com.sdl.web.content.client.odata.v2.edm.Schema implements Serializable {

    @EdmProperty(name = "SchemaId", nullable = false)
    private int schemaId;
    @EdmProperty(name = "Title", nullable = true)
    private String title;
    @EdmProperty(name = "PublicationId", nullable = false)
    private int publicationId;

    @EdmNavigationProperty(name = "Components", nullable = true)
    private List<Component> components;

    @Override
    public int getSchemaId() {
        return schemaId;
    }

    @Override
    public void setSchemaId(int schemaId) {
        this.schemaId = schemaId;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int getPublicationId() {
        return publicationId;
    }

    @Override
    public void setPublicationId(int publicationId) {
        this.publicationId = publicationId;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Schema s = (Schema) o;
        return (this.getSchemaId() == s.getSchemaId() && this.getPublicationId() == s.getPublicationId());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.getSchemaId();
        hash = 29 * hash + this.getPublicationId();
        return hash;
    }

}

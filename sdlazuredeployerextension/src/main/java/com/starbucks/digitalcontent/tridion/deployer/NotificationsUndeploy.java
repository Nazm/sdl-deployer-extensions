package com.starbucks.digitalcontent.tridion.deployer;

import com.tridion.configuration.Configuration;
import com.tridion.configuration.ConfigurationException;
import com.tridion.deployer.Processor;

public class NotificationsUndeploy extends AbstractDeployerExtension {
    public NotificationsUndeploy(Configuration config, Processor processor) throws ConfigurationException {
        super(config, processor);
    }
}

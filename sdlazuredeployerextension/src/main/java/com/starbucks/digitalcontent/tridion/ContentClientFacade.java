/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.tridion;

import com.sdl.odata.client.api.ODataClientQuery;
import com.sdl.web.content.client.ODataV2ClientQuery;
import com.starbucks.digitalcontent.model.Binary;
import com.starbucks.digitalcontent.model.Component;
import com.starbucks.digitalcontent.model.ComponentPresentation;
import com.starbucks.digitalcontent.model.Keyword;
import com.starbucks.digitalcontent.model.Page;
import com.starbucks.digitalcontent.model.Schema;
import com.starbucks.digitalcontent.model.Taxonomy;
import com.starbucks.digitalcontent.model.Template;
import com.tridion.util.TCMURI;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pebeemst
 */
public class ContentClientFacade {

    private static final int ITEM_TYPE_CATEGORY = 512;
    
    private final ContentClient _client;
    private final Map<String, com.sdl.web.content.client.odata.v2.edm.Keyword> _taxonomies
            = Collections.synchronizedMap(new HashMap<>());
    private final boolean _verbose;

    
    public ContentClientFacade(ContentClient instance, boolean verbose) {
        _client = instance;
        _verbose = verbose;
    }
    
    public List<Binary> getBinaryVariants(TCMURI id) {
        if(null == id || id.getItemType() != 16) { return null; }
        
        try {
            String filter = String.format("PublicationId eq %1$d AND BinaryId eq %2$d", id.getPublicationId(), id.getItemId());
            ODataClientQuery query = new ODataV2ClientQuery.Builder()
                    .withEntityType(com.sdl.web.content.client.odata.v2.edm.BinaryVariant.class)
                    .withFilteringParameter(filter)
                    .build();
            
            List<?> entities = _client.getEntities(com.sdl.web.content.client.odata.v2.edm.BinaryVariant.class.getName(), query);
            if(null != entities && !entities.isEmpty()) {
                List<Binary> variants = Collections.synchronizedList(new ArrayList<>());
                entities.forEach(entity -> {
                    com.sdl.web.content.client.odata.v2.edm.BinaryVariant edm = (com.sdl.web.content.client.odata.v2.edm.BinaryVariant) entity;
                    Binary variant = this.getBinaryVariantFromEdm(edm);
                    if(null!=variant) {
                        variants.add(variant);
                    }                    
                });
                return variants;
            }
            return null;
            
        } catch(Exception e) {
            return null;
        }
        
    }
    
    public ComponentPresentation getComponentPresentation(TCMURI componentId, TCMURI templateId) {
        if(null == componentId || null == templateId) { return null; }
        if(componentId.getItemType()!=16 || templateId.getItemType()!=32) { return null; }
        
        try {
            List<String> expands = new ArrayList<>();
            if(_verbose) {
                expands.addAll(Arrays.asList(
                    "Component/CustomMetas",
                    "Component/Keywords/CustomMetas",
                    "Component/Schema",
                    "Template"
                ));
            } else {
                expands.addAll(Arrays.asList(
                    "Component/Keywords",
                    "Component/Schema",
                    "Template"
                ));
            }

            ODataClientQuery query = new ODataV2ClientQuery.Builder()
                    .withEntityType(com.sdl.web.content.client.odata.v2.edm.ComponentPresentation.class)
                    .withEntityParameterMap("PublicationId", Integer.toUnsignedString(componentId.getPublicationId()))
                    .withEntityParameterMap("ComponentId", Integer.toUnsignedString(componentId.getItemId()))
                    .withEntityParameterMap("TemplateId", Integer.toUnsignedString(templateId.getItemId()))
                    .withExpandParameters(String.join(",", expands))
                    .build();
            com.sdl.web.content.client.odata.v2.edm.ComponentPresentation cp =
                    (com.sdl.web.content.client.odata.v2.edm.ComponentPresentation) _client.getEntity(com.sdl.web.content.client.odata.v2.edm.ComponentPresentation.class.getName(), query);
            return this.getComponentPresentationFromEdm(cp);
            
        } catch(Exception e) {
            
            return null;
        }
    }

    public Page getPage(TCMURI id) {
        
        if(null == id || id.getItemType() != 64) { return null; }
        
        try {
            List<String> expands = new ArrayList<>();
            if(_verbose) {
                expands.addAll(Arrays.asList(
                    "CustomMetas",
                    "ComponentPresentations/Component/CustomMetas",
                    "ComponentPresentations/Component/Keywords/CustomMetas",
                    "ComponentPresentations/Component/Schema",
                    "ComponentPresentations/Template",
                    "Keywords/CustomMetas"
                ));
            } else {
                expands.addAll(Arrays.asList(
                    "ComponentPresentations/Component/Keywords",
                    "ComponentPresentations/Component/Schema",
                    "ComponentPresentations/Template",
                    "Keywords"
                ));
            }
            
            ODataClientQuery query = new ODataV2ClientQuery.Builder()
                    .withEntityType(com.sdl.web.content.client.odata.v2.edm.Page.class)
                    .withEntityParameterMap("PublicationId", Integer.toUnsignedString(id.getPublicationId()))
                    .withEntityParameterMap("ItemId", Integer.toUnsignedString(id.getItemId()))
                    .withExpandParameters(String.join(",", expands))
                    .build();

            com.sdl.web.content.client.odata.v2.edm.Page page = 
                    (com.sdl.web.content.client.odata.v2.edm.Page) _client.getEntity(com.sdl.web.content.client.odata.v2.edm.Page.class.getName(), query);
            return this.getPageFromEdm(page);
            
        } catch(Exception e) {
           System.err.println(e.getLocalizedMessage());
           return null;
        }
    }
    
    public Schema getSchema(TCMURI id) {
        if(id == null || id.getItemType() != 8) { return null; }
        
        ODataV2ClientQuery.Builder builder = new ODataV2ClientQuery.Builder()
                .withEntityType(com.starbucks.digitalcontent.tridion.edm.Schema.class)
                .withEntityParameterMap("PublicationId", Integer.toUnsignedString(id.getPublicationId()))
                .withEntityParameterMap("SchemaId", Integer.toUnsignedString(id.getItemId()));
        if(_verbose) {
            builder.withExpandParameters("Components");
        }
        ODataClientQuery query = builder.build();
        com.starbucks.digitalcontent.tridion.edm.Schema schema = (com.starbucks.digitalcontent.tridion.edm.Schema)
                _client.getEntity(com.starbucks.digitalcontent.tridion.edm.Schema.class.getName(), query);
        
        return this.getSchemaFromEdm(schema);
    }

    public Taxonomy getTaxonomy(TCMURI id) {
        
        if(null == id || ITEM_TYPE_CATEGORY != id.getItemType()) { return null; }
        
        String key = id.toString();
        com.sdl.web.content.client.odata.v2.edm.Keyword rootKeyword = _taxonomies.getOrDefault(key, null);
        if(rootKeyword == null) {
            this.refreshTaxonomies(id.getPublicationId());
            rootKeyword = _taxonomies.getOrDefault(key, null);
        }
        return rootKeyword != null ? this.getTaxonomyFromEdm(rootKeyword) : null;
    }
    
    private Binary getBinaryVariantFromEdm(com.sdl.web.content.client.odata.v2.edm.BinaryVariant variant) {
        if(null == variant) { return null; }
        
        Binary result = new Binary();
        result.setId(String.format("tcm:%1$d-%2$d", variant.getPublicationId(), variant.getBinaryId()));
        result.setVariantId(variant.getVariantId());
        result.setMimeType(variant.getType());
        result.setTitle(variant.getPath());
        result.setUrl(variant.getUrlPath());
        return result;
    }
    
    private Page getPageFromEdm(com.sdl.web.content.client.odata.v2.edm.Page page) {
        if(null == page) { return null; }
        
        Page result = new Page();
        result.setId(String.format("tcm:%1$d-%2$d-64", page.getPublicationId(), page.getItemId()));
        result.setPath(page.getPagePath());
        result.setTitle(page.getTitle());
        result.setUrl(page.getUrl());
        
        if(page.getComponentPresentations() != null) {
            page.getComponentPresentations().forEach(cp -> {
                result.getComponentPresentations().add(this.getComponentPresentationFromEdm(cp));
            });
        }
        if(page.getKeywords() != null) {
            page.getKeywords().forEach(keyword -> {
                result.getKeywords().add(this.getKeywordFromEdm(keyword));
            });
        }

        if(_verbose) {
            result.setModified(this.getDateValueAsString(page.getModificationDate()));
            result.setPageTemplate(String.format("tcm:%1$d-%2$d-128", page.getPublicationId(), page.getTemplateId()));
            result.setVersion(this.getVersionAsString(page.getMajorVersion(), page.getMinorVersion()));
            result.setMetadata(this.getCustomMetasAsMap(page.getCustomMetas()));
        }
        
        return result;            
    }
    
    private ComponentPresentation getComponentPresentationFromEdm(com.sdl.web.content.client.odata.v2.edm.ComponentPresentation cp) {
        if(null == cp) { return null; }
        
        ComponentPresentation result = new ComponentPresentation();
        result.setComponent(this.getComponentFromEdm(cp.getComponent()));
        result.setTemplate(this.getTemplateFromEdm(cp.getTemplate()));
        
        return result;
    }
    
    private Component getComponentFromEdm(com.sdl.web.content.client.odata.v2.edm.Component component) {
        if(null == component) { return null; }
        
        Component result = new Component();
        result.setId(String.format("tcm:%1$d-%2$d", component.getPublicationId(), component.getItemId()));
        result.setTitle(component.getTitle());

        if(component.getKeywords() != null) {
            component.getKeywords().forEach(keyword -> {
                result.getKeywords().add(this.getKeywordFromEdm(keyword));
            });
        }
        
        if(_verbose) {
            result.setModified(this.getDateValueAsString(component.getModificationDate()));
            result.setVersion(this.getVersionAsString(component.getMajorVersion(), component.getMinorVersion()));
            result.setMetadata(this.getCustomMetasAsMap(component.getCustomMetas()));
        
            Schema xsd = this.getSchemaFromEdm((com.starbucks.digitalcontent.tridion.edm.Schema) component.getSchema());
            if(null != xsd) {
                result.setContentType(xsd.getTitle());
                result.setSchema(xsd.getId());
            }
        }
        
        return result;
    }
    
    private Keyword getKeywordFromEdm(com.sdl.web.content.client.odata.v2.edm.Keyword keyword) {
        if(null == keyword) { return null; }
        
        Keyword result = new Keyword();
        result.setId(String.format("tcm:%1$d-%2$d-%3$d", keyword.getPublicationId(), keyword.getId(), keyword.getItemType()));
        result.setTitle(keyword.getName());
        result.setKey(keyword.getKey());
        result.setTaxonomy(this.getTaxonomyTitle(keyword.getPublicationId(), keyword.getTaxonomyId()));
        result.setMetadata(this.getCustomMetasAsMap(keyword.getCustomMetas()));
        
        if(_verbose) {
            result.setTotalRelatedItems(keyword.getTotalRelatedItems());
        }

        return result;
    }
    
    private Schema getSchemaFromEdm(com.starbucks.digitalcontent.tridion.edm.Schema schema) {
        if(null == schema) { return null; }
        
        Schema result = new Schema();
        result.setId(String.format("tcm:%1$d-%2$d-8", schema.getPublicationId(), schema.getSchemaId()));
        result.setTitle(schema.getTitle());
        
        if(_verbose) {
            List<com.sdl.web.content.client.odata.v2.edm.Component> components = schema.getComponents();
            if(components!=null) {
                result.setNumberOfComponents(components.size());
            }
        }
        
        return result;
    }
    
    private Taxonomy getTaxonomyFromEdm(com.sdl.web.content.client.odata.v2.edm.Keyword keyword) {
        if(null == keyword) { return null; }
        
        Taxonomy result = new Taxonomy();
        result.setId(String.format("tcm:%1$d-%2$d-%3$d", keyword.getPublicationId(), keyword.getId(), keyword.getItemType()));
        result.setMetadata(null);
        result.setTaxonomy(null);
        result.setTitle(keyword.getName());
        
        if(_verbose) {
            result.setKey(keyword.getKey());
            result.setUsedForIdentification(keyword.isUsedForIdentification());
        }
        return result;
    }
    
    private Template getTemplateFromEdm(com.sdl.web.content.client.odata.v2.edm.Template template) {
        if(null == template) { return null; }
        
        Template result = new Template();
        result.setTitle(template.getTitle());
        
        if(_verbose) {
            result.setModified(this.getDateValueAsString(template.getModificationDate()));
        }
        
        return result;
    }
    
    private Map<String,ArrayList<String>> getCustomMetasAsMap(List<com.sdl.web.content.client.odata.v2.edm.CustomMeta> customMetas) {
        Map<String,ArrayList<String>> result = Collections.synchronizedMap(new HashMap<>());
        if (null != customMetas) {
            customMetas.forEach(cm -> {

                String key = cm.getKeyName();
                String value = null;

                switch (cm.getMetadataType().getColumnName()) {
                    case "stringValue":
                        value = cm.getStringValue();
                        break;
                    case "dateValue":
                        value = this.getDateValueAsString(cm.getDateValue());
                        break;
                    case "floatValue":
                        value = this.getFloatValueAsString(cm.getFloatValue());
                        break;
                    default:
                        break;
                }
                if (!result.containsKey(key)) {
                    result.put(key, new ArrayList<>());
                }
                ((ArrayList<String>) result.get(key)).add(value);
            });
        }
        return result;
    }
    
    private String getDateValueAsString(ZonedDateTime value) {
        return null != value ? value.toOffsetDateTime().toString() : null;
    }
    
    private String getFloatValueAsString(BigDecimal value) {
        return null != value ? value.toPlainString() : null;
    }
    
    private String getVersionAsString(int major, int minor) {
        return String.join(".", Integer.toUnsignedString(major), Integer.toUnsignedString(minor));
    }

    private String getTaxonomyTitle(int publicationId, int taxonomyId) {
        
        TCMURI taxonomyURI = new TCMURI(publicationId, taxonomyId, ITEM_TYPE_CATEGORY, 0);
        String key = taxonomyURI.toString();
        com.sdl.web.content.client.odata.v2.edm.Keyword taxonomy = _taxonomies.getOrDefault(key, null);

        if(null == taxonomy) {
            refreshTaxonomies(taxonomyURI.getPublicationId());
            taxonomy = _taxonomies.getOrDefault(key, null);
        }
        
        return null != taxonomy ? taxonomy.getName(): "";
    }
    
    private void refreshTaxonomies(int publicationId) {
        
        String filter = String.format("PublicationId eq %1$d AND ItemType eq %2$d", publicationId, ITEM_TYPE_CATEGORY);
        
        ODataClientQuery query = new ODataV2ClientQuery.Builder()
                .withEntityType(com.sdl.web.content.client.odata.v2.edm.Keyword.class)
                .withFilteringParameter(filter)
                .build();

        List<?> categories = _client.getEntities(com.sdl.web.content.client.odata.v2.edm.Keyword.class.getName(), query);
        if(null != categories) {
            categories.forEach((c) -> {
                com.sdl.web.content.client.odata.v2.edm.Keyword category =
                        (com.sdl.web.content.client.odata.v2.edm.Keyword) c;
                
                TCMURI key = new TCMURI(category.getPublicationId(), category.getId(), category.getItemType(), 0);
                this._taxonomies.put(key.toString(), category);
            });
        }
    }
    
}

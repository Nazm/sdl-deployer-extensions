package com.starbucks.digitalcontent.tridion.edm;

import com.sdl.odata.api.edm.annotations.EdmEntity;
import com.sdl.odata.api.edm.annotations.EdmEntitySet;
import com.sdl.odata.api.edm.annotations.EdmNavigationProperty;
import com.sdl.odata.api.edm.annotations.EdmProperty;
import com.sdl.web.content.client.odata.v2.edm.ComponentPresentation;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

@EdmEntity(name = "Template", namespace = "Tridion.ContentDelivery", key = {"ItemId", "PublicationId"})
@EdmEntitySet(name = "Templates")
public final class Template extends com.sdl.web.content.client.odata.v2.edm.Template implements Serializable {

    @EdmProperty(name = "TemplatePriority", nullable = false)
    private int templatePriority;
    @EdmProperty(name = "OutputFormat", nullable = true)
    private String outputFormat;
    @EdmProperty(name = "Title", nullable = true)
    private String title;
    @EdmProperty(name = "ItemId", nullable = false)
    private int itemId;
    @EdmProperty(name = "PublicationId", nullable = false)
    private int publicationId;
    @EdmProperty(name = "MajorVersion", nullable = true)
    private int majorVersion;
    @EdmProperty(name = "MinorVersion", nullable = true)
    private int minorVersion;
    @EdmProperty(name = "OwningPublication", nullable = true)
    private int owningPublication;
    @EdmProperty(name = "CreationDate", nullable = true)
    private ZonedDateTime creationDate;
    @EdmProperty(name = "InitialPublishDate", nullable = true)
    private ZonedDateTime initialPublishDate;
    @EdmProperty(name = "LastPublishDate", nullable = true)
    private ZonedDateTime lastPublishDate;
    @EdmProperty(name = "ModificationDate", nullable = true)
    private ZonedDateTime modificationDate;
    @EdmProperty(name = "Author", nullable = false)
    private String author;

    @EdmNavigationProperty(name = "ComponentPresentations", nullable = true)
    private List<ComponentPresentation> componentPresentations;

    public List<ComponentPresentation> getComponentPresentations() {
        return componentPresentations;
    }

    public void setComponentPresentations(List<ComponentPresentation> componentPresentations) {
        this.componentPresentations = componentPresentations;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    @Override
    public ZonedDateTime getInitialPublishDate() {
        return initialPublishDate;
    }

    @Override
    public int getItemId() {
        return itemId;
    }

    @Override
    public ZonedDateTime getLastPublishDate() {
        return lastPublishDate;
    }

    @Override
    public int getMajorVersion() {
        return majorVersion;
    }

    @Override
    public int getMinorVersion() {
        return minorVersion;
    }

    @Override
    public ZonedDateTime getModificationDate() {
        return modificationDate;
    }

    @Override
    public String getOutputFormat() {
        return outputFormat;
    }

    @Override
    public int getOwningPublication() {
        return owningPublication;
    }

    @Override
    public int getPublicationId() {
        return publicationId;
    }

    @Override
    public int getTemplatePriority() {
        return templatePriority;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public void setInitialPublishDate(ZonedDateTime initialPublishDate) {
        this.initialPublishDate = initialPublishDate;
    }

    @Override
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    @Override
    public void setLastPublishDate(ZonedDateTime lastPublishDate) {
        this.lastPublishDate = lastPublishDate;
    }

    /**
     *
     * @param majorVersion
     */
    @Override
    public void setMajorVersion(int majorVersion) {
        this.majorVersion = majorVersion;
    }

    @Override
    public void setMinorVersion(int minorVersion) {
        this.minorVersion = minorVersion;
    }

    @Override
    public void setModificationDate(ZonedDateTime modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public void setOutputFormat(String outputFormat) {
        this.outputFormat = outputFormat;
    }

    @Override
    public void setOwningPublication(int owningPublication) {
        this.owningPublication = owningPublication;
    }

    @Override
    public void setPublicationId(int publicationId) {
        this.publicationId = publicationId;
    }

    @Override
    public void setTemplatePriority(int templatePriority) {
        this.templatePriority = templatePriority;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Template t = (Template) o;
        return (this.getItemId() == t.getItemId() && this.getPublicationId() == t.getPublicationId());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.getItemId();
        hash = 67 * hash + this.getPublicationId();
        return hash;
    }
}

package com.starbucks.digitalcontent.tridion;

import com.sdl.odata.client.BasicODataClientQuery;
import com.sdl.odata.client.URLConnectionRequestPropertiesBuilder;
import com.sdl.odata.client.api.ODataClient;
import com.sdl.web.client.impl.OAuthTokenProvider;
import com.sdl.web.discovery.datalayer.model.BaseURL;
import com.sdl.web.discovery.datalayer.model.PublicationMapping;
import com.sdl.web.discovery.datalayer.model.WebApplication;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import org.apache.http.client.utils.URIUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DiscoveryClient {

    private final ODataClient odataClient;
    private final OAuthTokenProvider tokenProvider;
    private static final Logger LOG = LoggerFactory.getLogger(DiscoveryClient.class);

    protected DiscoveryClient(ODataClient odataClient, Map<String, String> clientProperties) {

        this.odataClient = odataClient;

        OAuthTokenProvider provider = null;
        try {
            Properties props = new Properties();
            props.putAll(clientProperties);
            provider = new OAuthTokenProvider(props);
        } catch (Exception e) {
            LOG.warn("Exception occurred initializing OAuth token provider: " + e.getLocalizedMessage());
        }
        this.tokenProvider = provider;
    }

    public List<URI> getWebApplicationBaseURLs(int publicationId) {
        List<URI> result = Collections.synchronizedList(new ArrayList<>());

        BasicODataClientQuery mappingsQuery = new BasicODataClientQuery.Builder()
                .withEntityType(PublicationMapping.class)
                .withFilterMap("id", Integer.toString(publicationId))
                .withExpandParameters("WebApplications")
                .build();
        Map<String, String> requestProperties = (new URLConnectionRequestPropertiesBuilder())
                .withAccessToken(this.getOAuthToken())
                .build();

        List<PublicationMapping> mappings = this.odataClient
                .getEntities(requestProperties, mappingsQuery)
                .stream()
                .map(entity -> (PublicationMapping) entity)
                .collect(Collectors.toList());

        mappings.forEach((mapping) -> {
            WebApplication app = mapping.getWebApplication();

            List<BaseURL> baseURLs = null != app ? app.getBaseURLs() : null;
            String contextURL = null != app ? app.getContextUrl() : null;

            if (null != baseURLs) {
                baseURLs.forEach((baseURL) -> {
                    try {
                        URI appBaseURL = URIUtils.createURI(
                                baseURL.getProtocol(),
                                baseURL.getHost(),
                                baseURL.getPort(),
                                contextURL,
                                null,
                                null
                        );
                        if (null != appBaseURL) {
                            result.add(appBaseURL);
                        }
                    } catch (URISyntaxException ex) {
                        LOG.warn("Exception occurred parsing web application base URL: {}" + ex.getLocalizedMessage());
                    }
                });
            }
        });

        return result;
    }

    private String getOAuthToken() {
        return null != this.tokenProvider ? this.tokenProvider.getToken() : "";
    }
}

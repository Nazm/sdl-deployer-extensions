package com.starbucks.digitalcontent.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class Item {
    
    @JsonProperty()
    private String id;

    @JsonProperty()
    private String title;
    
    @JsonProperty()
    private String url;
    
    @JsonProperty()
    private String modified;
    
    @JsonProperty()
    private String version;

    public String getId() {
        return id;
    }

    public String getModified() {
        return modified;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }
    
    public String getVersion() {
        return this.version;
    }
   
    public void setId(String id) {
        this.id = id;
    }
    
    public void setModified(String modified) {
        this.modified = modified;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    
}

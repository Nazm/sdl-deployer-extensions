/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pebeemst
 */
public abstract class ItemWithMetadata extends Item {
        
    @JsonProperty()
    private Map<String,ArrayList<String>> metadata = Collections.synchronizedMap(new HashMap<String,ArrayList<String>>());

    public Map<String,ArrayList<String>> getMetadata() {
        return this.metadata;
    }
    
    public void setMetadata(Map<String,ArrayList<String>> metadata) {
        this.metadata = 
                metadata == null || metadata.isEmpty() ? null : Collections.synchronizedMap(metadata);
    }
    
}

package com.starbucks.digitalcontent.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class ItemWithMetadataAndKeywords extends ItemWithMetadata {
    
    @JsonProperty
    private final List<Keyword> keywords = Collections.synchronizedList(new ArrayList<>());
    
    public List<Keyword> getKeywords() {
        return this.keywords;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author pebeemst
 */
public class Taxonomy extends Keyword {
    
    @JsonProperty
    private boolean usedForIdentification;

    public boolean isUsedForIdentification() {
        return usedForIdentification;
    }

    public void setUsedForIdentification(boolean usedForIdentification) {
        this.usedForIdentification = usedForIdentification;
    }

}

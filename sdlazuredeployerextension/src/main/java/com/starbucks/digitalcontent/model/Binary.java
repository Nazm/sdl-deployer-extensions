/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.model;

/**
 *
 * @author pebeemst
 */
public class Binary extends Item {
    
    private String variantId;
    private String mimeType;

    public String getVariantId() {
        return variantId;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}

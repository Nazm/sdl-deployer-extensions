/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.model.gson;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.util.List;

/**
 *
 * @author pebeemst
 */
 public class CollectionAdapter implements JsonSerializer<List<?>> {
    @Override
    public JsonElement serialize(List<?> src, Type typeOfSrc, JsonSerializationContext context) {
        if(null == src || src.isEmpty()) {
            return null;
        }
        JsonArray array = new JsonArray();
        src.forEach((child)-> {
            JsonElement element = context.serialize(child);
            array.add(element);
        });
        return array;
    }
}
        

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author pebeemst
 */
public class Component extends ItemWithMetadataAndKeywords {
    @JsonProperty
    private String contentType;
    
    @JsonProperty
    private String schema;
    
    public String getContentType() {
        return this.contentType;
    }
    public String getSchema() {
        return this.schema;
    }
    public void setContentType(String type) {
        this.contentType = type;
    }
    public void setSchema(String schemaName) {
        this.schema = schemaName;
    }
}

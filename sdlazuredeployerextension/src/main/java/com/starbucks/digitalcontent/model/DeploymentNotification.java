/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author pebeemst
 */
public class DeploymentNotification {
    
    @JsonProperty
    private String action;
    
    @JsonProperty
    private String transactionId;
    
    @JsonProperty    
    private Publication publication;
    
    @JsonProperty   
    private final List<Binary> binaries = Collections.synchronizedList(new ArrayList<>());
    
    @JsonProperty
    private final List<Schema> contentTypes = Collections.synchronizedList(new ArrayList<>());

    @JsonProperty
    private final List<ComponentPresentation> componentPresentations = Collections.synchronizedList(new ArrayList<>());

    @JsonProperty    
    private final List<Page> pages = Collections.synchronizedList(new ArrayList<>());

    @JsonProperty
    private final List<Keyword> taxonomies = Collections.synchronizedList(new ArrayList<>());

    public String getAction() {
        return action;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public Publication getPublication() {
        return publication;
    }

    public List<Page> getPages() {
        return pages;
    }

    public List<ComponentPresentation> getComponentPresentations() {
        return componentPresentations;
    }

    public List<Keyword> getTaxonomies() {
        return taxonomies;
    }

    public List<Binary> getBinaries() {
        return binaries;
    }

    public List<Schema> getContentTypes() {
        return contentTypes;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author pebeemst
 */
public class Publication {
    
    @JsonProperty
    private List<URI> baseURIs = Collections.synchronizedList(new ArrayList<>());
    @JsonProperty
    private String key;
    @JsonProperty
    private String title;
    @JsonProperty
    private String id;

    public List<URI> getBaseURIs() {
        return baseURIs;
    }

    public String getKey() {
        return key;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public void setBaseURIs(List<URI> baseURIs) {
        this.baseURIs = baseURIs;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(String id) {
        this.id = id;
    }
}

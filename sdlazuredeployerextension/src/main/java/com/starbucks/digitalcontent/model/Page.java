/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author pebeemst
 */
public class Page extends ItemWithMetadataAndKeywords {
    
    @JsonProperty()
    private String path;
    
    @JsonProperty()
    private String pageTemplate;
    
    @JsonProperty()
    private final List<ComponentPresentation> componentPresentations = Collections.synchronizedList(new ArrayList<>());

    public List<ComponentPresentation> getComponentPresentations() {
        return this.componentPresentations;
    }
    
    public String getPageTemplate() {
        return this.pageTemplate;
    }
    
    public String getPath() { 
        return this.path;
    }
    
    public void setPageTemplate(String template) {
        this.pageTemplate = template;
    }
    
    public void setPath(String path) {
        this.path = path;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import scala.math.BigInt;

/**
 *
 * @author pebeemst
 */
public class Keyword extends ItemWithMetadata {
    
    @JsonProperty
    private String key;
    @JsonProperty
    private String taxonomy;
    @JsonProperty
    private Integer totalRelatedItems;

    public String getKey() {
        return key;
    }

    public String getTaxonomy() {
        return taxonomy;
    }
    
    public Integer getTotalRelatedItems() {
        return totalRelatedItems;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
    }
    
    public void setTotalRelatedItems(Integer count) {
        this.totalRelatedItems = count;
    }
    
}

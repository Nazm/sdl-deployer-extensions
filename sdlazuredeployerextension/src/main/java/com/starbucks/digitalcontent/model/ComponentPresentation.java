/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starbucks.digitalcontent.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author pebeemst
 */
public class ComponentPresentation {
    
    @JsonProperty
    private Component component;
    
    @JsonProperty
    private Template template;
    
    public Component getComponent() {
        return component;
    }

    public Template getTemplate() {
        return template;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

}
